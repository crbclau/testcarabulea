#include "Deck.h"

static const size_t numberOfCards = 81;

void Deck::shuffleCards()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, numberOfCards);

	for (Card* card : m_deck)
	{
		int swapIndex = dis(gen);
		std::swap(card, m_deck[swapIndex]);
	}
}

Deck::Deck() : m_deckSize(numberOfCards)
{
}

Deck::~Deck()
{
}

