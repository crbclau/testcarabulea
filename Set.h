#pragma once
#include "Card.h"

class Set : public Card
{
private:
	Card m_playCard[3];
	size_t m_numberOfCards = 3;
public:
	Set(const Card& firstCard, const Card& secondCard, const Card& thirdCard);
	Set(const Set& otherSet);
	Set& operator = (const Set& otherSet);
	bool validateNumber();
	bool validateSymbol();
	bool validateShading();
	bool validateColor();
	bool checkSet() ;

	~Set();

};

