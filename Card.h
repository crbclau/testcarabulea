#pragma once
#include <string>

class Card
{
private:
	size_t m_number;
	std::string m_symbol;
	std::string m_shading;
	std::string m_color;
public:
	enum class Number
	{
		ONE = 1,
		TWO,
		THREE
	};

	enum class Symbol
	{
		DIAMOND = 1,
		SQUIGGLE,
		OVAL
	};

	enum class Shading
	{
		SOLID = 1,
		STRIPED,
		OPEN
	};

	enum class Color
	{
		RED = 1 ,
		GREEN,
		BLUE
	};
	size_t& getNumber();
	std::string& getSymbol();
	std::string& getShading();
	std::string& getColor();
	Card(const size_t& number = 0, const  std::string& symbol = "nullstr", const std::string& shading = "nullstr", const std::string& color = "nullstr");
	Card(const Card& otherCard);
	Card& operator = (const Card& otherCard); 
	~Card();

};

