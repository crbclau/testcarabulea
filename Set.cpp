#include "Set.h"
#include "Card.h"

Set & Set::operator=(const Set & otherSet)
{
	for (size_t index = 0; index < m_numberOfCards; ++index)
	{
		this->m_playCard[index] = otherSet.m_playCard[index];
	}

	return *this;
}

bool Set::validateNumber() 
{
	bool firstCheck = (m_playCard[0].getNumber() == m_playCard[1].getNumber()) || (m_playCard[0].getNumber() != m_playCard[1].getNumber());
	bool secondCheck = (m_playCard[0].getNumber() == m_playCard[2].getNumber()) || (m_playCard[0].getNumber() != m_playCard[2].getNumber());
	bool thirdCheck = (m_playCard[1].getNumber() == m_playCard[2].getNumber()) || (m_playCard[1].getNumber() != m_playCard[2].getNumber());

	return (firstCheck == true && secondCheck == true  && thirdCheck == true);
}

bool Set::validateSymbol() 
{
	bool firstCheck = (m_playCard[0].getSymbol() == m_playCard[1].getSymbol()) || (m_playCard[0].getSymbol() != m_playCard[1].getSymbol());
	bool secondCheck = (m_playCard[0].getSymbol() == m_playCard[2].getSymbol()) || (m_playCard[0].getSymbol() != m_playCard[2].getSymbol());
	bool thirdCheck = (m_playCard[1].getSymbol() == m_playCard[2].getSymbol()) || (m_playCard[1].getSymbol() != m_playCard[2].getSymbol());

	return (firstCheck == true && secondCheck == true && thirdCheck == true);
}

bool Set::validateShading() 
{
	bool firstCheck = (m_playCard[0].getShading() == m_playCard[1].getShading()) || (m_playCard[0].getShading() != m_playCard[1].getShading());
	bool secondCheck = (m_playCard[0].getShading() == m_playCard[2].getShading()) || (m_playCard[0].getShading() != m_playCard[2].getShading());
	bool thirdCheck = (m_playCard[1].getShading() == m_playCard[2].getShading()) || (m_playCard[1].getShading() != m_playCard[2].getShading());

	return (firstCheck == true && secondCheck == true && thirdCheck == true);
}

bool Set::validateColor() 
{
	bool firstCheck = (m_playCard[0].getColor() == m_playCard[1].getColor()) || (m_playCard[0].getColor() != m_playCard[1].getColor());
	bool secondCheck = m_playCard[0].getColor() == m_playCard[2].getColor() || (m_playCard[0].getColor() != m_playCard[2].getColor());
	bool thirdCheck = (m_playCard[1].getColor() == m_playCard[2].getColor()) || (m_playCard[1].getColor() != m_playCard[2].getColor());

	return (firstCheck == true && secondCheck == true && thirdCheck == true);
}

bool Set::checkSet() 
{
	bool numberCheck = validateNumber() == true;
	bool colorCheck = validateColor() == true ;
	bool symbolCheck = validateSymbol() == true;
	bool shadingCheck = validateShading() == true;

	return (numberCheck == true && colorCheck == true && symbolCheck == true && shadingCheck == true);
}

Set::Set(const Card & firstCard, const Card & secondCard, const Card & thirdCard)
{
	m_playCard[0] = firstCard;
	m_playCard[1] = secondCard;
	m_playCard[2] = thirdCard;
}

Set::Set(const Set & otherSet)
{
	for (size_t index = 0; index < m_numberOfCards; ++index)
	{
		m_playCard[index] = otherSet.m_playCard[index];
	}
}


Set::~Set()
{
}
