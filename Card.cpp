#include "Card.h"

Card::Card(const size_t & number, const std::string & symbol, const std::string & shading, const std::string & color) :
	m_number(number),
	m_symbol(symbol),
	m_shading(shading),
	m_color(color)
{
}

Card::Card(const Card & otherCard) :
	m_number(otherCard.m_number),
	m_symbol(otherCard.m_symbol),
	m_shading(otherCard.m_shading),
	m_color(otherCard.m_color)
{
}

Card & Card::operator=(const Card & otherCard)
{
	this->m_number = otherCard.m_number;
	this->m_symbol = otherCard.m_symbol;
	this->m_shading = otherCard.m_shading;
	this->m_color = otherCard.m_color;

	return *this;
}

size_t & Card::getNumber()
{
	return m_number;
}

std::string & Card::getSymbol()
{
	return m_symbol;
}

std::string & Card::getShading()
{
	return m_shading;
}

std::string & Card::getColor()
{
	return m_color;
}


Card::~Card()
{
}
