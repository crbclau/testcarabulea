#pragma once
#include <iostream>
#include <fstream>
#include <string>

class Log
{
private:
	static std::ofstream out;
public:
	static void print(const std::string& log);
};
