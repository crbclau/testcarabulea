#pragma once
#include "Card.h"
#include<array>
#include<random>
#include <algorithm> 

class Deck : public Card
{
private:
	size_t m_deckSize;
	std::vector<Card*> m_deck;
public:
	Deck();
	void shuffleCards();
	~Deck();

};

